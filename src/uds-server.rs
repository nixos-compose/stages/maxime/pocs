// file tokio API doc
// https://docs.rs/tokio/latest/tokio/net/struct.UnixStream.html
use tokio::net::UnixListener;
use tokio::io::Interest;
use std::error::Error;
use std::io;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let listener = UnixListener::bind("/tmp/uds-server.sock").unwrap();
    loop {
        match listener.accept().await {
            Ok((stream, _addr)) => {
                println!("new client!");
                // loop {
                    let ready = stream.ready(Interest::READABLE | Interest::WRITABLE).await?;
                    
                    if ready.is_readable() {
                        let mut data = vec![0; 1024];
                        
                        // Try to read data, this may still fail with `WouldBlock`
                        // if the readiness event is a false positive.
                        match stream.try_read(&mut data) {
                            Ok(n) => {
                               let s = String::from_utf8_lossy(&data);
                                println!("read {} bytes", n);
                                println!("read {}",s);
                            }
                            Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
                                continue;
                            }
                            Err(e) => {
                                return Err(e.into());
                            }          
                        }
                    }
                //}            
            }
            Err(e) => { /* connection failed */ }
        }
        println!("Ended with this client......");
    }
}



// use tokio::io::Interest;
// use tokio::net::UnixStream;
// use std::error::Error;
// use std::io;

// use std::os::unix::net::UnixStream as StdUnixStream;

// #[tokio::main]
// async fn main() -> Result<(), Box<dyn Error>> {
//     // let dir = tempfile::tempdir().unwrap();
//     // let bind_path = dir.path().join("bind_path");
//     // let stream = UnixStream::connect(bind_path).await?;
//     let std_stream = StdUnixStream::connect("/tmp/uds-server-socket")?;
//     std_stream.set_nonblocking(true)?;
//     let stream = UnixStream::from_std(std_stream)?;


    
//     loop {
//         let ready = stream.ready(Interest::READABLE | Interest::WRITABLE).await?;

//         if ready.is_readable() {
//             let mut data = vec![0; 1024];
//             // Try to read data, this may still fail with `WouldBlock`
//             // if the readiness event is a false positive.
//             match stream.try_read(&mut data) {
//                 Ok(n) => {
//                     println!("read {} bytes", n);        
//                 }
//                 Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
//                     continue;
//                 }
//                 Err(e) => {
//                     return Err(e.into());
//                 }
//             }

//         }

//         if ready.is_writable() {
//             // Try to write data, this may still fail with `WouldBlock`
//             // if the readiness event is a false positive.
//             match stream.try_write(b"hello world") {
//                 Ok(n) => {
//                     println!("write {} bytes", n);
//                 }
//                 Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
//                     continue;
//                 }
//                 Err(e) => {
//                     return Err(e.into());
//                 }
//             }
//         }
//     }
// }
