# pocs

## helloworld-tutorial

- from https://github.com/hyperium/tonic/tree/master/examples
```bash
cargo run --bin helloworld-server
cargo run --bin helloworld-client
```

## src/uds-server.rs 
- simple uds server with tokio
```bash
cargo run --bin uds-server
echo "yop" | socat - UNIX-CONNECT:/tmp/uds-server.sock
```

## Next steps (out-of-order)
- [ ] https://github.com/hyperium/tonic/blob/master/examples/routeguide-tutorial.md
- [ ] uds-server + client grpc -> server grpc (no stream)
- [ ] uds-client
- [ ] server grpc + uds-client
- [ ] uds-server + client grpc -> server grpc + uds-client (no stream)
- [ ] uds-server + client grpc -> server grpc + uds-client (stream)
- [ ] uds-server + client grpc -> server grpc + uds-client (bidirection stream)
- [ ] uds identification SO_PEERCRED
- [ ] add logging
- [ ] add tls
- [ ] add authentication
- [ ] config
- [ ] systemd service uds-conveyor-client / uds-conveyor-proxy
- [ ] add authentication by host ?
- [ ] doc
- [ ] test
- [ ] features list
